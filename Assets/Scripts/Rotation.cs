﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Rotation : MonoBehaviour {
	
	private float StockValueA;
	private float StockValueB;
	private float StockValueC;

	public Animator anim;
	public Button StartButton;
	private System.Random rnd;
	public Text VialCountText;
	public int VialCount;

	
	public Button ButtonA;
	public Button ButtonB;
	public Button ButtonC;

	public GameObject Popup;
	
	public GameObject DoneButton;
	public GameObject UndoButton;
	public GameObject SliderGameObject;

	public Text MaxValueText;
		
	public Text PopupText;

	public List<string> RouletteInstances;

	public Slider StockSlider;

	public Text StockText;
	public Text SelectedStockText;

	String SelectedStock;

	void Start () {
		
		StockValueA = 0f;
		StockValueB = 0f;
		StockValueC = 0f;

		anim.speed = 0.0f;
		
		SelectedStock = "";
		rnd = new System.Random ();
	}

	public void StartAnimation () {
		
		DoneButton.SetActive (false);
		UndoButton.SetActive (false);
		StockSlider.interactable = false;

		if (VialCount > 0 && (StockValueA > 0f || StockValueB > 0f || StockValueC > 0f)) {
			//Debug.Log ("Start Animation");
			anim.speed = 2.0f;
			
			StartButton.interactable = false;
			ButtonA.interactable = false;
			ButtonB.interactable = false;
			ButtonC.interactable = false;

			--VialCount;

			VialCountText.text = "Vial Count: " + VialCount;

			int StopAnimationTime = rnd.Next (3, 7);

			float randomFactor = rnd.Next (0, 10);
			float StopTime = StopAnimationTime + (randomFactor / 10);

			Debug.Log ("Spinning Time: " + StopTime);

			Invoke ("StopAnimation", StopTime);
		}

		else {

			if (VialCount == 0) {
				Popup.SetActive (true);
				PopupText.text = "You've no vial to invest stock.";
			}

			else {
				Popup.SetActive (true);
				PopupText.text = "Please invest a stock (A, B, or C) first.";
			}
		}
	}

	public void StopAnimation () {
//		Debug.Log ("Stop Animation");
//		anim.speed = 0.0f;

		InvokeRepeating ("StopRouletteSmoothly", 0.0f, 0.5f);
	}

	void StopRouletteSmoothly () {
		//Debug.Log ("Stop Roulette Smoothly");
		if (anim.speed < 0.1f) {
			anim.speed = 0.0f;

			
			int index = (int) (gameObject.GetComponent<RectTransform>().eulerAngles.z / 22.5f);
			
			Debug.Log ("On Roulette: " + RouletteInstances[index]);
			
			Debug.Log ("Selected Stock: " + SelectedStock);
			
			if (RouletteInstances[index].Equals ("A") && StockValueA > 0f) {
				Popup.SetActive (true);
				PopupText.text = "Congratulations! You've won a stock A (" + StockValueA * 2 + " $)!";
				StockSlider.maxValue += (StockValueA * 2);
				StockSlider.value = StockSlider.minValue;
				StockText.text = StockSlider.value + " $";
			}
			
			else if (RouletteInstances[index].Equals ("B") && StockValueB > 0f) {
				Popup.SetActive (true);
				PopupText.text = "Congratulations! You've won a stock B (" + StockValueB * 4 + " $)!";
				StockSlider.maxValue += (StockValueB * 4);
				StockSlider.value = StockSlider.minValue;
				StockText.text = StockSlider.value + " $";
			}
			
			else if (RouletteInstances[index].Equals ("C") && StockValueC > 0f) {
				Popup.SetActive (true);
				PopupText.text = "Congratulations! You've won a stock C (" + StockValueC * 16 + " $)!";
				StockSlider.maxValue += (StockValueC * 16);
				StockSlider.value = StockSlider.minValue;
				StockText.text = StockSlider.value + " $";
			}
			
			else {
				Popup.SetActive (true);
				PopupText.text = "You've lost the stock!"; 
			}

			
			MaxValueText.text = "Max: " + StockSlider.maxValue + "$";

			SelectedStock = "";
			StockValueA = 0f;
			StockValueB = 0f;
			StockValueC = 0f;

			
			ColorBlock cb = ButtonA.colors;
			cb.normalColor = Color.white;
			cb.highlightedColor = Color.white;
			ButtonA.colors = cb;

			cb = ButtonB.colors;
			cb.normalColor = Color.white;
			cb.highlightedColor = Color.white;
			ButtonB.colors = cb;

			cb = ButtonC.colors;
			cb.normalColor = Color.white;
			cb.highlightedColor = Color.white;
			ButtonC.colors = cb;

			StartButton.interactable = true;
			ButtonA.interactable = true;
			ButtonB.interactable = true;
			ButtonC.interactable = true;
			CancelInvoke("StopRouletteSmoothly");

			return;
		}

		if (anim.speed > 0.2f) {
			anim.speed -= 0.2f;
		}

		else {
			anim.speed = 0.0f;
		}
//		if (
//		anim.speed -= 0.2f;
	}

	public void SetSelectedStock (string Stock) {

		SelectedStock = Stock.Trim();

		if (SelectedStock.Equals ("A")) {

			if (ButtonA.colors.normalColor == Color.red) {
				UndoButton.SetActive (true);
				DoneButton.SetActive (false);
				StockSlider.interactable = false;

				StockSlider.value = StockSlider.minValue;

				SelectedStockText.text = "Selected Stock:\n";
			}

			else {
				
				SelectedStockText.text = "Selected Stock:\n" + SelectedStock;
				DoneButton.SetActive (true);
				UndoButton.SetActive (false);
				StockSlider.interactable = true;
			}
		}

		else if (SelectedStock.Equals ("B")) {
			
			if (ButtonB.colors.normalColor == Color.red) {
				UndoButton.SetActive (true);
				DoneButton.SetActive (false);
				StockSlider.interactable = false;
				
				StockSlider.value = StockSlider.minValue;
			
				SelectedStockText.text = "Selected Stock:\n";
			}
			
			else {
				SelectedStockText.text = "Selected Stock:\n" + SelectedStock;
				DoneButton.SetActive (true);
				UndoButton.SetActive (false);
				StockSlider.interactable = true;
			}
		}

		else if (SelectedStock.Equals ("C")) {
			
			if (ButtonC.colors.normalColor == Color.red) {
				UndoButton.SetActive (true);
				DoneButton.SetActive (false);
				StockSlider.interactable = false;
				
				StockSlider.value = StockSlider.minValue;

				SelectedStockText.text = "Selected Stock:\n";
			}
			
			else {
				SelectedStockText.text = "Selected Stock:\n" + SelectedStock;
				DoneButton.SetActive (true);
				UndoButton.SetActive (false);
				StockSlider.interactable = true;
			}
			
		}

		Debug.Log ("Selected Stock: " + SelectedStock);
	}


	public void OnDoneButtonClicked () {
		
		if (SelectedStock.Equals ("A")) {
			
			if (StockSlider.value == StockSlider.minValue) {
				
				SelectedStock = "";
				StockValueA = StockSlider.value;
				Popup.SetActive (true);
				PopupText.text = "Please select value > 0 $"; 
				SelectedStockText.text = "Selected Stock:\n";
				return;
			}

			SelectedStock = "";
			StockValueA = StockSlider.value;
			StockSlider.maxValue -= StockSlider.value;
			StockText.text = StockSlider.value + " $";
			StockSlider.value = StockSlider.minValue;


			ColorBlock cb = ButtonA.colors;
			cb.normalColor = Color.red;
			cb.highlightedColor = Color.red;
			ButtonA.colors = cb;
				//ButtonA.colors.normalColor = Color.red;
		}
		
		else if (SelectedStock.Equals ("B")) {
			
			if (StockSlider.value == StockSlider.minValue) {
				
				SelectedStock = "";
				StockValueB = StockSlider.value;
				Popup.SetActive (true);
				PopupText.text = "Please select value > 0 $"; 
				SelectedStockText.text = "Selected Stock:\n";
				return;
			}

			SelectedStock = "";
			StockValueB = StockSlider.value;
			StockSlider.maxValue -= StockSlider.value;
			StockText.text = StockSlider.value + " $";
			StockSlider.value = StockSlider.minValue;
			//ButtonB.colors.normalColor = Color.red;


			ColorBlock cb = ButtonB.colors;
			cb.normalColor = Color.red;
			cb.highlightedColor = Color.red;
			ButtonB.colors = cb;
		}
		
		else if (SelectedStock.Equals ("C")) {
			
			if (StockSlider.value == StockSlider.minValue) {
				
				SelectedStock = "";
				StockValueC = StockSlider.value;
				Popup.SetActive (true);
				PopupText.text = "Please select value > 0 $"; 
				SelectedStockText.text = "Selected Stock:\n";
				return;
			}

			SelectedStock = "";
			StockValueC = StockSlider.value;
			StockSlider.maxValue -= StockSlider.value;
			StockText.text = StockSlider.value + " $";
			StockSlider.value = StockSlider.minValue;
			//ButtonC.colors.normalColor = Color.red;


			ColorBlock cb = ButtonC.colors;
			cb.normalColor = Color.red;
			cb.highlightedColor = Color.red;
			ButtonC.colors = cb;
			
		}
		
		MaxValueText.text = "Max: " + StockSlider.maxValue + "$";
	}

	
	public void OnUndoButtonClicked () {
		
		if (SelectedStock.Equals ("A")) {

			StockSlider.maxValue += StockValueA;
			StockValueA = 0f;
			StockText.text = StockSlider.value + " $";
			ColorBlock cb = ButtonA.colors;
			cb.normalColor = Color.white;
			cb.highlightedColor = Color.white;
			ButtonA.colors = cb;
		}
		
		else if (SelectedStock.Equals ("B")) {

			StockSlider.maxValue += StockValueB;
			StockValueB = 0f;
			StockText.text = StockSlider.value + " $";
			//ButtonB.colors.normalColor = Color.white;
			ColorBlock cb = ButtonB.colors;
			cb.normalColor = Color.white;
			cb.highlightedColor = Color.white;
			ButtonB.colors = cb;
		}
		
		else if (SelectedStock.Equals ("C")) {

			StockSlider.maxValue += StockValueC;
			StockValueC = 0f;
			StockText.text = StockSlider.value + " $";
			//ButtonC.colors.normalColor = Color.white;
			ColorBlock cb = ButtonC.colors;
			cb.normalColor = Color.white;
			cb.highlightedColor = Color.white;
			ButtonC.colors = cb;
			
		}

		MaxValueText.text = "Max: " + StockSlider.maxValue + "$";
	}

	public void SetStockValue (float value) {
		StockText.text = value + " $";
	}


}
